"""
Project from spring 2020, an attempt at a qa system
"""

import os, re, string, sys
import random
import traceback
from pprint import pprint
"""from dhtk.catalogs.gutenberg.data import GutenbergData
from dhtk.catalogs.gutenberg.book import GutenbergBook
from dhtk.catalogs.gutenberg.author import GutenbergAuthor
from dhtk.catalogs.gutenberg.texts import GutenbergTexts"""
import json
from bson import json_util
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from SPARQLWrapper import SPARQLWrapper, JSON, XML
from SPARQLWrapper.SPARQLExceptions import QueryBadFormed, EndPointNotFound
from pymongo import MongoClient
"""from flair.data import Sentence, Corpus
from flair.models import SequenceTagger"""



dirname = os.path.dirname(__file__)

alphabets= "([A-Za-z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov)"

class Qasystem:
    """A class for every python fan, youg and old alike!"""

    def __init__(self):
        # score is the normally printed score that the user see
        self.score = 0
        

        # connection to mongodb
        db_url = os.environ['MONGOLAB_URI']
        self.client = MongoClient(db_url)
        self.database = self.client["heroku_7t7rf39m"]
        self.books = self.database["books"]
        self.extractedDatabase = self.database["extractedData"]
        self.players = self.database["players"]

        books_cursor = self.books.find({})
        player_cursor = self.players.find({})

        self.books_data = list(books_cursor)
        self.player_data = list(player_cursor)
        

        #dump mongodb to file (because why not)
        with open(os.path.join(dirname,"metadata.json"), "w") as out:
            out.write(json_util.dumps(self.books_data))
        
        with open(os.path.join(dirname,"userdata.json"), "w") as out:
            out.write(json_util.dumps(self.player_data))

        # Loads database
        with open(os.path.join(dirname,"metadata.json"), "r") as inp:
            self.data = json_util.loads(inp.read())
        with open(os.path.join(dirname,"userdata.json"), "r") as inp:
            self.userdata = json_util.loads(inp.read())
        with open(os.path.join(dirname,"database.json"), "r") as inp:
            self.extractedData = json.load(inp)

        self.extractedDatabase.insert_many(self.extractedData)

        # the metascore of each kind of metadata is linked to the availability of completion questions for each kind of metadata for the user
        self.metascore = dict()
        for key in self.data[0].keys():
            self.metascore[key] = 1


    def _pushDatabase(self):
        """
        This private method pushes the data and userdata databases to the mangodb cloud.
        It is called at the termination of the program no matter what.
        It should NEVER be called in any other context whatsoever.
        :type return: None
        """

        self.books.delete_many({})
        self.players.delete_many({})

        self.books.insert_many(self.data)
        if len(self.userdata) > 0:
            self.players.insert_many(self.userdata)

    def getBooks(self, books=["Alice's Adventures in Wonderland", "The Fellowship of the Ring", "Dune", "La Légende des siècles", "L’Étranger", "The Eye of the World", "Pride and Prejudice", "A Brief History of Time"]):
        """
        This function is deprecated.
        This function uses sparql queries to get metadata on books available from dbpedia
        You can pass it a list of books or use the default list
        :type book: list
        :param book: A list of book titles
        :type return: list
        :param return: A list of metadata associated with the input books
        """
        sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        booksList = []

        # Sparql block
        for book in books:
            # For each book, create a sparql query
            booksQuery = ""
            booksQuery +="SELECT ?title ?author ?pubDate ?pubLoc ?publisher ?lang WHERE{"
            booksQuery +=f'?title foaf:name "{book}"@en .'
            booksQuery +="?title rdf:type bibo:Book ."
            booksQuery +="?title rdf:type dbo:Book ."
            booksQuery +="?title rdf:type dbo:WrittenWork ."
            booksQuery +="?title dbp:language ?lang ."
            booksQuery +="OPTIONAL{?title dbo:author ?author} ."
            booksQuery +="OPTIONAL{?title dbp:country ?pubLoc} ."
            booksQuery +="OPTIONAL{?title dbp:published ?pubDate} ."
            booksQuery +="OPTIONAL{?title dbp:pubDate ?pubDate} ."
            booksQuery +="OPTIONAL{?title dbo:releaseDate ?pubDate} ."
            booksQuery +="OPTIONAL{?title dbo:publisher ?publisher} ."
            booksQuery +=""
            booksQuery +="}"
            sparql.setQuery(booksQuery)
            try:
                # Ask dbpedia with the sparql
                sparql.setReturnFormat(JSON)
                ret = sparql.query()
                booksDict = ret.convert()
                booksList.append(booksDict)
            except:
                raise
        print(booksQuery)
        # Results processing block
        returnList = []
        for item in booksList:
            # Extract the relevant data from the result dictionary from sparql
            for result in item["results"]["bindings"]:
                tmpdict = {}
                for key in result.keys():
                    tmpstring = result[key]["value"].split("/")[-1]
                    if key == "pubDate":
                        # Because pubDates are complete madness on dbpedia, we need to process them differently
                        # Basically, the approach is that the first 4 consecutive numbers found must be the year of the first publication
                        # It's an assumption, and therefore, this method is not optimal, but computationally costless
                        try:
                            tmpint = int(tmpstring[:4])
                            tmpdict[key] = tmpint
                        except ValueError:
                            tmpint = re.search("\d{4}", tmpstring)
                            tmpint = tmpint.group(0)
                            tmpdict[key] = int(tmpint)
                    else:
                        tmpstring = re.sub("_", " ", tmpstring)
                        tmpstring = re.sub("\(.+\)", "", tmpstring).strip()
                        tmpdict[key] = tmpstring

            # This small block identifies missing data by comparison with other entries in the database
            for k in item["head"]["vars"]:
                if k not in tmpdict.keys():
                    tmpdict[k] = None
            returnList.append(tmpdict)
                

        return returnList

    def getAllBooks(self):
        """
        similar to getBook, but returns every single book of dbpedia
        :type return: list
        :param return: a nested list of dictionaries, each of which represent a book and it's associated metadata
        """


        sparql = SPARQLWrapper("http://dbpedia.org/sparql")

        query = ""
        query+="SELECT ?title ?pubDate ?pubLoc ?lang ?author ?publisher"
        query+="(GROUP_CONCAT(?genre;SEPARATOR=',') as ?genres)"
        query+="WHERE{"
        query+="?o foaf:name ?title ."
        query+="OPTIONAL{?o dbo:literaryGenre ?genre} ."
        query+="?o dbp:language ?lang ."
        query+="OPTIONAL{?o dbo:author ?author} ."
        query+="OPTIONAL{?o dbp:country ?pubLoc} ."
        query+="OPTIONAL{?o dbp:published ?pubDate} ."
        query+="OPTIONAL{?o dbp:pubDate ?pubDate} ."
        query+="OPTIONAL{?o dbo:releaseDate ?pubDate} ."
        query+="OPTIONAL{?o dbo:publisher ?publisher} ."
        query+="filter( contains(str(?lang), 'English' ))"
        query+="filter( !contains(str(?title), 'Cthulhu' ))"
        query+="{ ?o a dbo:Book } UNION { ?o a bibo:Book } UNION { ?o a dbo:WrittenWork }"
        query+="}LIMIT 100"

        print(query)
        sparql.setQuery(query)

        try:
            # Ask dbpedia with the sparql
            print("Contacting endpoint")
            sparql.setReturnFormat(JSON)
            ret = sparql.query()
            print("Query done, converting to dict and processing.")
            booksDict = ret.convert()
        except:
            raise

        returnList = list()
        
        print(len(booksDict["results"]["bindings"]))

        for result in booksDict["results"]["bindings"]:
            tmpdict = dict()
            if result["lang"]["value"] != "English":
                continue
            for key in result.keys():
                if type(result[key]["value"]) == list and len(result[key]["value"]) > 0:
                    tmplist = list()
                    for it in result[key]["value"]:
                        tmpstring = it.split("/")[-1]
                        tmpstring = re.sub("_", " ", tmpstring)
                        tmpstring = re.sub("\(.+\)", "", tmpstring).strip()
                        tmplist.append(tmpstring)
                    tmpdict[key] = tmplist

                elif type(result[key]["value"]) == list and len(result[key]["value"]) == 0:
                    tmpdict[key] = None
                else:
                    tmpstring = result[key]["value"].split("/")[-1]
                    if key == "pubDate":
                        # Because pubDates are complete madness on dbpedia, we need to process them differently
                        # Basically, the approach is that the first 4 consecutive numbers found must be the year of the first publication
                        # It's an assumption, and therefore, this method is not optimal, but computationally costless
                        tmpint = re.search("\d{4}", tmpstring)
                        if tmpint:
                            tmpint = tmpint.group(0)
                            tmpdict[key] = int(tmpint)
                        else:
                            tmpdict[key] = None
                    
                    elif key == "lang":
                        pass

                    else:
                        tmpstring = re.sub("_", " ", tmpstring)
                        tmpstring = re.sub("\(.+\)", "", tmpstring).strip()
                        if tmpstring == "":
                            tmpdict[key] = None
                        else:
                            tmpdict[key] = tmpstring

            # This small block identifies missing data by comparison with other entries in the database
            for k in booksDict["head"]["vars"]:
                if k not in tmpdict.keys():
                    tmpdict[k] = None


            returnList.append(tmpdict)

        # for some reason, some "lang" tags appear in the database, because they are perfectly useless, we delete them with the upmost prejudice
        for item in returnList:
            del(item["lang"])
        
        returnList = [item for item in returnList if item["title"] != None]

        print("Done, writing to metadata.json")

        with open("metadata.json", "w") as out:
            json.dump(returnList, out)

        return returnList

    def obtainText(self, book):
        """
        Checks if the book given as a parameter exists in Gutenberg and, if so, returns the full text
        :type book: tuple
        :param book: tuple with the title and author's first and last name as first, second and third elements respectively
        :type return: string
        :param reutn: the full body text of the book
        """
        #instanciations for DHTK
        gutenberg_search = GutenbergData("http://dhtk.unil.ch:3030/gutenberg/sparql")
        gutenberg_texts = GutenbergTexts()
        dhtk_book = gutenberg_search.search_by_title_and_author(book[0],book[1],book[2])
        if dhtk_book == []:
            return False
        # Because DHTK does not allow for perfect matching, this is necessary
        actual_book = [b for b in dhtk_book if b["title"] == book[0]]
        if len(actual_book) > 1:
            actual_book = actual_book[0]

        gut_book = GutenbergBook(
            title=actual_book["title"],
            gutenberg_id=actual_book["book_id"],
            author=GutenbergAuthor(
                gutenberg_id=actual_book["author_id"],
                name=actual_book["author"]
            )
        )

        full_text = gutenberg_texts.get_clean_text(gut_book)
        return full_text

    def obtainAllTexts(self):
        """
        This method extracts all the available texts from the books in the books.json file and store them in the texts folder.

        """
        with open("books.json", "r") as inp:
            books = json.load(inp)
        gutenberg_search = GutenbergData("http://dhtk.unil.ch:3030/gutenberg/sparql")

        for book in books:
            if "Poetry" in book["genres"]:
                continue

            gutenberg_texts = GutenbergTexts()
            print(book["title"])
            if type(book["author"]) == str:
                author_name = book["author"].split(" ")
            else:
                author_name = book["author"][0].split(" ")
            try:
                dhtk_book = gutenberg_search.search_by_title_and_author(book["title"],author_name[0],author_name[-1])
            except:
                continue

            if dhtk_book == []:
                continue

            actual_book = [b for b in dhtk_book if b["title"] == book["title"] or b["title"] in book["title"] or book["title"] in b["title"]]
            if actual_book == []:
                continue
            
            if type(actual_book) == list:
                actual_book = actual_book[0]

            gut_book = GutenbergBook(
                title=actual_book["title"],
                gutenberg_id=actual_book["book_id"],
                author=GutenbergAuthor(
                    gutenberg_id=actual_book["author_id"],
                    name=actual_book["author"]
                )
            )

            full_text = gutenberg_texts.get_clean_text(gut_book)

            path_to_book = os.path.join("texts", book["title"])
            if not os.path.exists(path_to_book):
                os.mkdir(path_to_book)

            file_name = f"{book['title']}.txt"
            with open(os.path.join(path_to_book, file_name), "w") as out:
                out.write(full_text)
                    
    def textCleaner(self, text, removepunctuation = False):
        """
        A method to clean and preprocess text
        :type text: string
        :type removepunctuation: bool
        """
        text = re.sub('[_;:—()]', ' ', text)
        text = re.sub("CHAPTER.+", "", text)
        if removepunctuation:
            text = re.sub('[.,;?!"]', ' ', text)
        text = " ".join(text.split())
        return text


    def neExtractor(self, text):
        """
        Extracts the named entities of a text
        :type text: string
        """

        tagger = SequenceTagger.load('ner')
        unique_entities = []

        for sentence in split_into_sentences(text):
            sentence = self.textCleaner(sentence)
            if not sentence == "":
                sentence = Sentence(sentence)
                tagger.predict(sentence)

            for entity in sentence.get_spans("ner"):
                if entity.text not in unique_entities:
                    unique_entities.append(entity.text)
            
        
        return unique_entities

    def allNeExtractor(self):
        """
        Extracts the named entities of all the texts
        """
        tagger = SequenceTagger.load('ner')

        for folder in os.listdir("texts"):
            try:
                unique_entities = []
                flag = False
                folder_path = os.path.join("texts", folder)
                for f in os.listdir(folder_path):
                    file_path = os.path.join(folder_path, f)
                    print(file_path)
                    if f.endswith(".txt"):
                        with open(file_path, "r") as inp:
                            text = inp.read()
                    if f.endswith(".json"):
                        flag = True

                if not flag:
                    for sentence in split_into_sentences(text):
                        sentence = self.textCleaner(sentence)
                        if not sentence == "":
                            sentence = Sentence(sentence)
                            tagger.predict(sentence)
                        else:
                            continue
                        for entity in sentence.get_spans("ner"):
                            if entity.text not in unique_entities:
                                unique_entities.append(entity.text)

                    output_path = os.path.join(folder_path, "nes.json")

                    with open(output_path, "w") as out:
                        json.dump(unique_entities,out)
            except NotADirectoryError:
                continue

    def informationExtractor(self, text, nes, title):
        """
        Attempts to extract information about a given ne in a text
        Returns : a list of tagged ne's
        """
        characters = set()
        locations = set()
        for ne in nes:
            for sentence in split_into_sentences(text):
                cleaned_sentence = self.textCleaner(sentence)
                # the approach is a naive but efficient one, if an entity says something, it must be a character,
                #  if something goes in or to or is from, it is assumed to be a location
                # later, the entity classified are filtered anyways
                #  because of this, we miss a lot of data, but we are certain that they are what we want (which is more important)
                match = re.search(" {0} (say|said|says)".format(ne), cleaned_sentence)
                match2 = re.search("(said|say|says)( the)? {0} ".format(ne), cleaned_sentence)
                match3 = re.search("(in|from|to) {0} ".format(ne), cleaned_sentence)

                if match:
                    characters.add(ne)
                if match2:
                    characters.add(ne)
                if match3:
                    locations.add(ne)

        locations = locations - characters
        
        characters = removeRedundancy(characters)
        locations = removeRedundancy(locations)
                
        # print("Characters:")
        # print(characters)
        # print("locations:")
        # print(locations)

        returnDict = {
            "title":title,
            "characters":list(characters),
            "locations":list(locations)
        }
        return returnDict

    def allInformationExtractor(self):
        """
        classifies all nes of all the texts into characters and locations
        """
        for folder in os.listdir("texts"):
            print(folder)
            if not os.path.exists(os.path.join("extractedData", folder)+".json"):
                try:
                    folder_path = os.path.join("texts", folder)
                    for f in os.listdir(folder_path):
                        file_path = os.path.join(folder_path, f)
                        if f.endswith(".txt"):
                            with open(file_path, "r") as inp:
                                text = inp.read()
                        if f.endswith(".json"):
                            with open(file_path, "r") as inp:
                                nes = json.load(inp)
                    
                    if text and nes:
                        characters = set()
                        locations = set()
                        for ne in nes:
                            try:
                                for sentence in split_into_sentences(text):
                                    cleaned_sentence = self.textCleaner(sentence)
                                    cleaned_ne = self.textCleaner(ne, removepunctuation=True)
                                    match = re.search(" {0} (say|said|says)".format(cleaned_ne), cleaned_sentence,re.IGNORECASE)
                                    match2 = re.search("(said|say|says)( the)? {0} ".format(cleaned_ne), cleaned_sentence,re.IGNORECASE)
                                    match3 = re.search("(in|from|to) {0} ".format(cleaned_ne), cleaned_sentence,re.IGNORECASE)

                                    if match:
                                        characters.add(cleaned_ne)
                                    if match2:
                                        characters.add(cleaned_ne)
                                    if match3:
                                        locations.add(cleaned_ne)
                                    
                                    locations = locations - characters
                            except:
                                continue
                        characters = removeRedundancy(characters)
                        locations = removeRedundancy(locations)

                        outDict = {
                            "title":folder,
                            "characters":list(characters),
                            "locations":list(locations)
                        }
                        output_path = os.path.join("extractedData", folder)
                        output_path += ".json"
                        with open(output_path, "w") as out:
                            json.dump(outDict, out)
                except:
                    continue

    def checkExistence(self, concept, nature):
        """
        bookExtraction daughter method, shouldn't be called directly if possible
        Check if a given concept exists in dbpedia and return it and it's origin work (as well as any straggler data)
        Returns a list of URI which points to the original works in dbpedia
        """
        sparql = SPARQLWrapper("http://dbpedia.org/sparql")

        if nature == "character":
            booksQuery = ""
            booksQuery +="SELECT ?name ?book ?abs WHERE{"
            booksQuery +="?o foaf:name ?name ."
            booksQuery +="?o rdf:type dbo:FictionalCharacter ."
            booksQuery +="?o dbo:firstAppearance ?book ."
            booksQuery +="?o dbo:abstract ?abs ."
            booksQuery +=f'filter( regex(str(?name), "{concept}" ))'
            booksQuery +='filter( lang(?abs) = "en")'
            booksQuery +="}"
            sparql.setQuery(booksQuery)
        elif nature == "location":
            booksQuery = ""
            booksQuery+="SELECT ?name ?book ?type ?abs WHERE{"
            booksQuery+="?o dbp:name ?name ."
            booksQuery+="?o rdf:type ?type ."
            booksQuery+="optional{?o dbp:source ?book} ."
            booksQuery+="?o dbo:abstract ?abs ."
            booksQuery+="filter(?type = yago:WikicatFictionalCountries || ?type = yago:yago:WikicatFictionalPlanets || ?type = dbo:Country || ?type = dbo:Planet)"
            booksQuery+="filter(lang(?abs) = 'en')"
            booksQuery+=f'filter( regex(str(?name), "{concept}"))'
            booksQuery+="}"
            sparql.setQuery(booksQuery)
        try:
            # Ask dbpedia with the sparql
            sparql.setReturnFormat(JSON)
            ret = sparql.query()
            booksDict = ret.convert()
        except:
            return None

        returnList = list()
        for result in booksDict["results"]["bindings"]:
            if "book" in result:
                returnList.append(
                    dict(
                        book = result["book"]["value"],
                        name = result["name"]["value"],
                        abs = result["abs"]["value"]
                    )
                )
            else:
                returnList.append(
                    dict(
                        book = "",
                        name = result["name"]["value"],
                        abs = result["abs"]["value"]
                    )
                )

        return returnList
    def alignDatabases(self):
        """
        This method creates entries for all books in the database in the player collection, and wipes all accumulated answers from the player collection
        """
        self.players.delete_many({})
        list_of_book_titles = set()
        for entry in self.books_data:
            list_of_book_titles.add(entry["title"])
            print(entry["title"])
        for title in list_of_book_titles:
            self.players.insert({"title": title})
            print(title)

    def bookExtraction(self):
        """
        This method uses it's daughter method to extract relevant character and location data from dbpedia of the books in the extractedData folder
        """
        returnList = list()
        # the extractedData folder is were each book gets a file where the preliminary extracted data is stored,
        #  we use this extracted data as a starting point for our search of dbpedia
        for datafile in os.listdir("extractedData"):
            print(datafile)
            foundCharacters = list()
            foundLocations = list()
            with open(os.path.join("extractedData", datafile), "r") as inp:
                data = json.load(inp)

            for char in data["characters"]:
                char = self.textCleaner(char, True)
                charData = self.checkExistence(char, "character")
                if charData:
                    for char2 in charData:
                        if re.search(data["title"], char2["book"]):
                            foundCharacters.append(char2)
            nameList = [char["name"] for char in foundCharacters]
            uniqueNames = removeRedundancy(set(nameList))
            uniqueChars = list ()

            for char in foundCharacters:
                if char["name"] in uniqueNames and not re.search("ABC", char["abs"]):
                    uniqueChars.append(
                        dict(
                            name = char["name"],
                            abs = char["abs"]
                            )
                        )


            for loc in data["locations"]:
                loc = self.textCleaner(loc, True)
                locData = self.checkExistence(loc, "location")
                if locData:
                    for loc2 in locData:
                        if re.search(data["title"], loc2["book"]):
                            foundLocations.append(loc2)
            nameList = [loc["name"] for loc in foundLocations]
            uniqueNames = removeRedundancy(set(nameList))
            uniqueLocs = list()

            for loc in foundLocations:
                if loc["name"] in uniqueNames:
                    uniqueLocs.append(
                        dict(
                            name = loc["name"],
                            abs = loc["abs"]
                            )
                        )
            if uniqueChars or uniqueLocs:
                returnList.append(
                    dict(
                        title = data["title"],
                        characters = uniqueChars,
                        locations = uniqueLocs
                    )
                )

        with open("database.json", "w") as out:
            json.dump(returnList, out)
            
        return returnList
        
    def countNulls(self):
        """
        This utility counts the number of null (missing data)
        """
        nr_of_null = 0
        nr_of_data = 0
        for book in self.books_data:
            for key in book.keys():
                if book[key] == None:
                    nr_of_null+=1
                if book[key] != None:
                    nr_of_data+=1
        
        return f"Nr Null : {nr_of_null}, Nr data : {nr_of_data}"
def dumpToJson(file, dict):
    """
    Updates a .json file with a new one
    """
    # this is a small utility, it's not worth commenting
    file_path = os.path.join(dirname,file)
    if os.path.exists(file_path):
        os.remove(file_path)

    with open(file_path, "w") as output:
        json.dump(dict, output)
        
def removeRedundancy(s):
    """
    Remove elements of a set that are subsets of one or more other elements
    (for instance in s={"12","1"} the element "1" is a subset of "12" and will be removed)
    """
    elements_to_remove = set()
    for el1 in s:
        for el2 in s:
            if el1 in el2 and not el1 == el2:
                elements_to_remove.add(el1)

    return s - elements_to_remove

def distance(dictionary):
    """
    Calculates the distance between the top two values of a given dict of the form
    {"key1": int1, "key2": int2, ...}
    :return: int
    """
    values = list(dictionary.values())
    if len(values) <= 1:
        return 0
    top = values.pop(values.index(max(values)))
    top2 = values.pop(values.index(max(values)))
    
    return top - top2


def split_into_sentences(text):
    """
    This method splits a text into sentences by tracking every special cases, it is a necessary step as feeding an entire novel to flair at once
    tends to overload it.
    Credits go to MR. Harsha Laxman of stackoverflow.
    """
    text = " " + text + "  "
    text = text.replace("\n"," ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = re.sub(websites,"<prd>\\1",text)
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + alphabets + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + alphabets + "[.]"," \\1<prd>",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("<prd>",".")
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]

    return sentences

    
def main():
    # instanciate the class
    qa = Qasystem()
    try:
        print(qa.countNulls())
    except KeyboardInterrupt:
        print("Program interrupted, shutting down...")
        # qa._pushDatabase()
        sys.exit(0)
        


if __name__ == "__main__":
    main()