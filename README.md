
# ISH Project Spring 2020


1. Augmenter la complexité des questions qui peuvent êtres posées sur la base des données structurées de DBpedia. 24.03.2020 (Worst case scenario 21.03.2020)
    1. La question typique actuelle est : "When was first published {book}?", le but est d'obtenir des questions du style "Between {book1} and {book2} which one was published first?", des questions plus difficiles peuvent également être ajoutées, du style "Which of these books was published in 1983?"
2. Augmenter la nature des données de la base, en ajoutant des données propres au livre (comme les personnages en jeu, le nombre de chapitres, etc.) quand cela est possible. Cela se passe pas l'utilisation de Gutenberg et de techniques de NLP pour récupérer des données textuelles non structurées. 14.04.2020 (worst case scenario 28.04.2020)
    1. Le but est la production de questions du style "From which book series Arda is the main setting?" ou "In Hamlet, which character speaks this line?
3. Augmenter la base de donnée des questions du jeu en ajoutant des entrées de manière systématique (plutôt que sur la base d'une liste). 21.04.2020 (worst case scenario 12.05.2020)
    1. Le but est de pouvoir obtenir de DBpedia l'ensemble des données existantes (métadonnés des livres), ainsi que d'obtenir quelles métadonnées sont manquantes (ou aberrantes) dans d'autres livres présents dans DBpedia, afin d'augmenter le nombre de questions possibles et de préparer le module suivant.
4. Ajouter des questions "de complétion", c'est a dire des questions à laquelle le jeu ne connaît pas la réponse servant à remplir les trous dans la base de donnée (c.a.d compléter les métadonneés manquantes dans le DBpedia). 05.05.2020 (worst case scenario 02.06.2020)
    1. Le but étant d'exploiter la sagesse des foules pour remplir les trous mis en évidence dans le point précédent. La vérification de la véracité des réponses passe par des inférences statistiques pondérées.
