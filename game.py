"""
Project from spring 2020, an attempt at a qa system
"""

import os, re, string, sys
import random
import traceback
from pprint import pprint
import json
from bson import json_util
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from pymongo import MongoClient
# import streamlit as st

dirname = os.path.dirname(__file__)



class QA:
    """A class for every python fan, youg and old alike!"""

    def __init__(self):

        # score is the normally printed score that the user see
        self.score = 0
        self.question_number = 0

        # connection to mongodb
        db_url = os.environ['MONGOLAB_URI']
        self.client = MongoClient(db_url)
        self.database = self.client["heroku_7t7rf39m"]
        self.books = self.database["books"]
        self.extractedDatabase = self.database["extractedData"]
        self.players = self.database["players"]
        self.stats = self.database["stats"]


        self.stats.find_one_and_update({"stats": "any"}, {"$inc":{"NrOfGames": 1}}, upsert=True)

        books_cursor = self.books.find({})
        extracted_cursor = self.extractedDatabase.find({})
        player_cursor = self.players.find({})

        self.data = list(books_cursor)
        self.extractedData = list(extracted_cursor)
        self.userdata = list(player_cursor)

        # the metascore of each kind of metadata is linked to the availability of completion questions for each kind of metadata for the user
        self.metascore = dict()
        for key in self.data[0].keys():
            if key != "_id":
                self.metascore[key] = 1
            

    def _pushDatabase(self):
        """
        This private method pushes the data and userdata databases to the mangodb cloud.
        It is called at the termination of the program no matter what.
        It should NEVER be called in any other context whatsoever.
        :type return: None
        """

        self.books.delete_many({})
        self.extractedDatabase.delete_many({})

        self.books.insert_many(self.data)
        self.extractedDatabase.insert_many(self.extractedData)
    def returnQuestion(self):
        """
        This method is used solely to interact with the flask app, it shoul never be called for any other reason.
        It functions like the first part of the askQuestion method, and return the question object created, instead
        of actually handling the questioning.
        """
        question_nature = random.choice(["single", "comparative", "difficult", "extracted", "completion"])

        if self.score > 5:
            question_nature = random.choice(["single", "comparative", "difficult"])
        if self.score > 10:
            question_nature = random.choice(["difficult", "extracted", "completion"])

        if question_nature == "single":
            # we call the createQuestions to create questions (with a default difficulty of 0, explicitely stated for clarity)
            question = self.createQuestions(difficulty=0)
        if question_nature == "comparative":
            # comparative questions get their own method because they function fundamentally differently from other kinds of questions
            question = self.createComparativeQuestions()
        if question_nature == "difficult":
            # difficult questions are difficulty level 1
            question = self.createQuestions(difficulty=1)
        if question_nature == "extracted":
            question = self.createExtractedQuestions()
        if question_nature == "completion":
            question = self.createQuestions(difficulty=2)

        self.stats.find_one_and_update({"stats": "any"}, {"$inc":{"NrOfGames": 1}}, upsert=True)
        return (question, question_nature)

    def askQuestion(self):
        """
        This method handles the questioning itself
        :type return: None
        """
        # For now, the question nature is chosen at random, for testing purposes
        # A simple question bascially asks metadata from a book
        # A comparative question compares two different books
        # A difficult question asks a book from it's metadata (which is arguably more difficult)
        question_nature = random.choice(["single", "comparative", "difficult", "extracted", "completion"])
        if self.score > 5:
            question_nature = random.choice(["single", "comparative", "difficult"])
        if self.score > 10:
            question_nature = random.choice(["difficult", "extracted", "completion"])
        try:
            if question_nature == "single":
                # we call the createQuestions to create questions (with a default difficulty of 0, explicitely stated for clarity)
                question = self.createQuestions(difficulty=0)
            if question_nature == "comparative":
                # comparative questions get their own method because they function fundamentally differently from other kinds of questions
                question = self.createComparativeQuestions()
            if question_nature == "difficult":
                # difficult questions are difficulty level 1
                question = self.createQuestions(difficulty=1)
            if question_nature == "extracted":
                question = self.createExtractedQuestions()
            if question_nature == "completion":
                question = self.createQuestions(difficulty=2)

            # menial initiations, and actual printing of the question text
            # m_correct_answer is the number of the correct answer for the question
            m_correct_answer = None
            print(question[0])
            correct_answer = question[1]


            # Comparative questions block
            if question_nature == "comparative":
                # comparative questions produce their own wrong answers (the third element from their return tuple)
                wrong_answers = question[2]
                # because we allow the user to answer using the number of the question, we choose randomly what this number is,
                # then we fill the other numbers with the wrong answers
                correct_answer_position = random.choice(range(1,4))
                for i in range(1,4):
                    if i == correct_answer_position:
                        print(f"{i} : {correct_answer}")
                    else:
                        print(f"{i} : {wrong_answers.pop()}")
                m_correct_answer = correct_answer_position

            # extracted question block
            if question_nature == "extracted":
                print(question[2])
                wrong_answers = list(question[3])
                correct_answer_position = random.choice(range(1,5))
                for i in range(1,5):
                    if i == correct_answer_position:
                        print(f"{i} : {correct_answer}")
                    else:
                        print(f"{i} : {wrong_answers.pop()}")
                m_correct_answer = correct_answer_position


            # Multiple answers block
            # by nature, completion question cannot provide a correct answer, thus, it is only possible to give a direct answer
            elif question_nature != "completion" and question_nature != "comparative":
                wrong_answers = []
                # this block choose 3 wrong answers that are not identical to the correct answer to serve as noise
                while(len(wrong_answers) < 3):
                    possible_wrong_answer = random.choice(self.data)[question[2]]
                    if possible_wrong_answer not in wrong_answers and possible_wrong_answer != None and possible_wrong_answer != correct_answer:
                        wrong_answers.append(possible_wrong_answer)

                # fundamentally the same idea as for the comparatice questions block, we choose a random correct position,
                # and fill the wrong positions with noise
                correct_answer_position = random.choice(range(1,5))
                for i in range(1,5):
                    if i == correct_answer_position:
                        print(f"{i} : {correct_answer}")
                    else:
                        print(f"{i} : {wrong_answers.pop()}")
                
                m_correct_answer = correct_answer_position

            



            # Answer processing block
            if question_nature != "completion":
                # User input
                answer = input("Enter the answer or the number of your answer: ")
                # answer = st.text_input("Enter the answer or the number of your answer: ")
                if answer == "exit()":
                    sys.exit(0)
                try:
                    # first we try to process the answer as an integer, if the answer is exactly equal to the answer or it's position in the multiple answers
                    # we call scoreHandler with True to process a correct answer
                    # elsewise, we call it with False to process an incorrect answer
                    ianswer = int(answer)
                    if ianswer == correct_answer or ianswer == m_correct_answer:
                        if question_nature != "extracted":
                            self.scoreHandler(True, question[2])
                        else:
                            self.scoreHandler(True)
                    else:
                        if question_nature != "extracted":
                            self.scoreHandler(False, question[2])
                        else:
                            self.scoreHandler(False)
                        print(f"The correct answer was {m_correct_answer} : {correct_answer}")
                except ValueError:
                    # if the user didn't give an int as an answer, we check if the answer is close enough to the answer (60% similar)
                    # to give some form of leeway in spelling mistakes
                    # we call scoreHandler with true if the answer is close enough
                    if not isinstance(correct_answer, int):
                        if fuzz.ratio(correct_answer, answer) > 60:
                            self.scoreHandler(True, question[2])
                        else:
                            self.scoreHandler(False, question[2])
                            print(f"The expected answer was : {correct_answer}")
                    else:
                        # This block should never be reached, but is kept just in case something catastrophic happens
                        self.scoreHandler(False, question[2])
                        print(f"The expected answer was : {correct_answer} and you gave me a string.")
            # completion question block
            else:
                # User input
                answer = input("Please enter your answer (or ? if don't know): ")
                if answer == "?":
                    print(f"Understood, abhorting question, your metascore is")
                    print(self.metascore)
                    
                else:
                    print("Thanks for your input, your metascore is")
                    print(question[1], question[2])
                    self.completer(answer, question[1], question[2])
                    print(self.metascore)

        except:
            # This block handles exceptions and errors in the code and the answers of the user
            print("That answer was either so completely incorrect it broke the game, or the game broke itself, abhorting question")
            print(traceback.format_exc())

        # finally, we call the function recursively to keep asking questions
        self.stats.find_one_and_update({"stats": "any"}, {"$inc":{"NrOfGames": 1}}, upsert=True)
        print("================================")
        self.askQuestion()

    def returnScoreHandler(self,result,question_destination=None):
        """
        This method is analogous to returnQuestion, in that it only serves as an interface with the flask app.
        It should never be called in any other context.
        :type result: bool
        :param result: a boolean representation of sucess in the question that called this method
        :type question_destination: string or None
        :param question_destination: the type of question asked, used to control the player's metascore about a specific kind of question.
         None signifies a non-standard question has been asked.
        """
        if result:
            self.score += 1
            if question_destination:
                try:
                    self.metascore[question_destination] += 1
                except TypeError:
                    print(traceback.format_exc)
            self.stats.find_one_and_update({"stats": "any"}, {"$inc":{"CorrectAnswers": 1}}, upsert=True)
            return f"Congratulation, you won! Your score is now {self.score}"
        elif not result:
            self.score -= 1
            if question_destination:
                try:
                    self.metascore[question_destination] -= 1
                except TypeError:
                    print(traceback.format_exc)
            self.stats.find_one_and_update({"stats": "any"}, {"$inc":{"IncorrectAnswers": 1}}, upsert=True)
            return f"Unfortunately, the answer was either wrong or too vague, your score is now {self.score}"
    def scoreHandler(self,result,question_destination=None):
        """
        Method that controls and uses the score. It is called after a question has been answered.
        :type result: bool
        :param result: a boolean representation of sucess in the question that called this method
        :type question_destination: string or None
        :param question_destination: the type of question asked, used to control the player's metascore about a specific kind of question.
         None signifies a non-standard question has been asked.
        """
        if result:
            self.score += 1
            if question_destination:
                self.metascore[question_destination] += 1
                print(f"Your metascore for {question_destination} is now {self.metascore[question_destination]}")
            self.stats.find_one_and_update({"stats": "any"}, {"$inc":{"CorrectAnswers": 1}}, upsert=True)                
            print(f"Congratulation, you won! Your score is now {self.score}")
        elif not result:
            self.score -= 1
            if question_destination:
                self.metascore[question_destination] -= 1
                print(f"Your metascore for {question_destination} is now {self.metascore[question_destination]}")
            self.stats.find_one_and_update({"stats": "any"}, {"$inc":{"CorrectAnswers": 1}}, upsert=True)
            print(f"Unfortunately, the answer was either wrong or too vague, your score is now {self.score}")


    def completer(self, answer, subject, question_nature):
        """
        This method handles the ansers given to a completion question, stores the data and eventually overrides the original database
        if sufficiently coherent answers are provided
        :type answer: string
        :param answer: the given user input
        :type subject: dict
        :param subject: the book in the database for which a question have been answered
        :type question_nature: string
        :param question_nature: the nature of the answer (title, pubDate, author, publisher, genres, pubLoc)
        :type return: None
        """
        threshold = 100



        document = self.players.find_one({"title": subject["title"]})
        if question_nature in document.keys():
            if answer in document[question_nature].keys():
                document[question_nature][answer] += 1
            else:
                document[question_nature][answer] = 1
        else:
            document[question_nature] = dict()
            document[question_nature][answer] = 1
        self.players.find_one_and_replace({"title": subject["title"]}, document)

        if document[question_nature][answer] >= threshold and distance(document[question_nature]) >= 30:
            metadocument = document
            metadocument[question_nature] = answer
            self.books.find_one_and_replace({"title": subject["title"]}, metadocument)

        self.stats.find_one_and_update({"stat": "any"}, {"$inc":{"TotalCompletionQuestions": 1}}, upsert=True)

    def createQuestions(self, difficulty = 0):
        """
        Method which generates standard questions asked by askQuestion.
        :type difficulty: int
        :param difficulty: a difficulty level of the question. 
         0 asks metadata about a book given it's title, 1 asks about a book's title given one of it's metadata.
         2 is a completion question, for which there is no multiple answers and no confirmation from the game.
        :type return: tuple
        :param return: a tuple which represents a question, of which the form is (questionstring, correct_answer, question_destination)
        """
        # subject is the book the question is going to be about, it's chosen at random amongst those in the database
        subject = random.choice(self.data)
        if difficulty == 0:
            # if the difficulty is 0, we take as an origin (the data given to the user) the title
            chosen_question_origin = "title"
            # the destination (what the user must find) can be any other data about the book, as long as this data is known
            possible_question_destination = [x for x in subject.keys() if x != chosen_question_origin and subject[x] != None and x != "_id"]

            if possible_question_destination:
                chosen_question_destination = random.choice(possible_question_destination)

            else:
                # if the book contains nothing but it's title (which apparently is not that uncommon), pick again
                return self.createQuestions()

        if difficulty == 1:
            # if the difficulty is 1, we take as an origin any data BUT the title (explicitely stated here)
            possible_questions_origin = [x for x in subject.keys() if subject[x] != None and x != "title" and x != "_id"]
            if possible_questions_origin:
                chosen_question_origin = random.choice(possible_questions_origin)
            else:
                return self.createQuestions(1)
            # and the destination is the title
            chosen_question_destination = "title"
        if difficulty == 2:
            # if the difficulty is 2, the question is a completion one, and as such, behaves like a difficulty 0 question
            # except it's destination is specifically not known by the database (subject[x] == None)
            # we also add a bit of filtering based on the player score, if his metascore of a subject is too low, it cannot be chosen
            chosen_question_origin = "title"
            possible_question_destination = [x for x in subject.keys() if x != chosen_question_origin and subject[x] == None and x != "_id"]

            if possible_question_destination:
                chosen_question_destination = random.choice(possible_question_destination)

            else:
                # if the book is complete, pick again
                return self.createQuestions(2)

        # we then make human readable questions from the question previously made with makeHumanQuestion
        question = self.makeHumanQuestion(origin_subject=chosen_question_origin, origin=subject[chosen_question_origin], destination_subject=chosen_question_destination)
        correct_answer = subject[chosen_question_destination]

        # we then retun a tuple which contains all the data we need
        if difficulty == 2:
            return (question, subject, chosen_question_destination)
        return (question, correct_answer, chosen_question_destination)



    def createComparativeQuestions(self):
        """
        Method which creates comparative questions between two different books.
        :type return: tuple
        :param return: a tuple representing a question, of the form: (questionstring, correct_answer, wrong_answer)
        """
        # For comparative questions, we take two books at random in the database
        subjects = random.sample(self.data, 2)
        try:
            # we then take any metadata that is explicitely an int (because comparing strings is fundamentally impossible)
            # in our set of data, it means the date exclusively
            possible_questions = [x for x in subjects[0].keys() if isinstance(subjects[0][x], int) and subjects[1][x]]
            chosen_question = random.choice(possible_questions)
            # for now, we use a PH model for the question, because i dont yet know what kind of data can be compared
            question = f"[PH] Between {subjects[0]['title']} and {subjects[1]['title']} which {chosen_question} is larger?'"

            # here we determine which one is bigger and which one is smaller (it might be anything)
            if subjects[0][chosen_question] > subjects[1][chosen_question]:
                correct_answer = subjects[0]["title"]
                wrong_answer = [subjects[1]["title"], "Neither"]
            elif subjects[0][chosen_question] < subjects[1][chosen_question]:
                correct_answer = subjects[1]["title"]
                wrong_answer = [subjects[0]["title"], "Neither"]
            else:
                # they might be equal too, although improbable if we talk about a number of words or pages, it might happen
                correct_answer = "Neither"
                wrong_answer = [subjects[0]["title"], subjects[1]["title"]]
            
            return (question, correct_answer, wrong_answer)
        except:
            return self.createComparativeQuestions()


    def createExtractedQuestions(self):
        """
        Create questions from the extracted data database
        :type return: tuple
        :param return: a tuple which represents a question of the form: (questionstring, correct_answer, edited_abstract, wrong_answers)
        """

        # This junk location list is for padding the wrong answers (because locations are often way less numerous in books)
        junkLocation = ["France", "England", "Switzerland", "Germany", "Italy", "Spain", "Ea", "Tamriel", "Arrakis"]
        try:
            subject = random.choice(self.extractedData)
            nature = random.choice(["characters", "locations"])
            print(nature)
            answer = random.choice(subject[nature])
            question = f"In {subject['title']} which of these {nature[:-1]} fit this description?"
            regex = answer["name"].replace(" ", "|")
            regex = f"({regex})"
            editedAbstract = re.sub(regex, "?", answer["abs"], flags=re.IGNORECASE)
            
            wrong_answers = set()
            i = 0
            while len(wrong_answers) < 4 and i < 50:
                i += 1
                potential_wrong_answer = random.choice(subject[nature])["name"]
                if not potential_wrong_answer == answer["name"]:
                    wrong_answers.add(potential_wrong_answer)
                    
            
            if len(wrong_answers) < 4:
                while len(wrong_answers) < 4:
                    potential_wrong_answer = random.choice(junkLocation)
                    if not random.choice(junkLocation) == answer["name"]:
                        wrong_answers.add(potential_wrong_answer)

            print(len(wrong_answers))
        except IndexError:
            return self.createExtractedQuestions()

        return (question, answer["name"], editedAbstract, wrong_answers)


    def makeHumanQuestion(self, origin_subject, origin, destination_subject):
        """
        Forms gramatical human-like questions from an origin and the destination subjects
        :type origin_subject: string
        :param origin_subject: the type of question
        :type origin: string
        :param origin: the data given to the player
        :type destination_subject: string
        :param destination_subject: that which the player must guess
        :type return: string
        """

        # this block handles easy questions
        if origin_subject == "title":
            if destination_subject == "author":
                output_string = f"Who wrote {origin}?"
            if destination_subject == "pubLoc":
                output_string = f"Where was first published {origin}?"
            if destination_subject == "publisher":
                output_string = f"What is the name of the publishing firm that first published {origin}?"
            if destination_subject == "pubDate":
                output_string = f"When was {origin} first published?"
            if destination_subject == "genres":
                output_string = f"What genre does {origin} belong to?"
        
        # Difficult questions
        if origin_subject == "pubDate":
            if destination_subject == "title":
                output_string = f"Among these books, which one was published in {origin}?"
            else:
                output_string = "Error in question processing (pubdate)"

        if origin_subject == "publisher":
            if destination_subject == "title":
                output_string = f"Among these books, which one was published by {origin}?"
            else:
                output_string = "Error in question processing (publisher)"
        
        if origin_subject == "pubLoc":
            if destination_subject == "title":
                output_string = f"Among these books, which one was published in {origin}?"
            else:
                output_string = "Error in question processing (pubLoc)"
        
        if origin_subject == "genres":
            if destination_subject == "title":
                output_string = f"Among these books, which one belongs to the {origin} genre(s)?"
            else:
                output_string = "Error in question processing (genres)"

        if origin_subject == "author":
            if destination_subject == "title":
                output_string = f"Among these books, which one was written by {origin}?"
            else:
                output_string = "Error in question processing (authors)"

        return output_string

def distance(dictionary):
    """
    Calculates the distance between the top two values of a given dict of the form
    {"key1": int1, "key2": int2, ...}
    :return: int
    """
    values = list(dictionary.values())
    if len(values) <= 1:
        return 0
    top = values.pop(values.index(max(values)))
    top2 = values.pop(values.index(max(values)))
    
    return top - top2
    
def main():
    # instanciate the class
    qa = QA()
    try:
        # qa.askQuestion()
        qa.completer("United States of America", {"title":"An Old Captivity"}, "pubLoc")
    except KeyboardInterrupt:
        print("Program interrupted, shutting down...")
        qa._pushDatabase()
        sys.exit(0)
        


if __name__ == "__main__":
    main()