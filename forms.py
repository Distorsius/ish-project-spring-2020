from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, TextAreaField, BooleanField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename



class answerForm(FlaskForm):
    input = StringField('Answer', validators=[DataRequired()])
    nextQuestion = BooleanField('Next question')
    submit = SubmitField('Submit')

class resultForm(FlaskForm):
    submit = SubmitField("Moar")