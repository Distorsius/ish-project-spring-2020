import os, re, string, sys
from flask import Flask, render_template, flash, redirect, request, send_file, send_from_directory, url_for
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import random
from forms import answerForm, resultForm
from game import QA

app = Flask(__name__)
app.config['SECRET_KEY'] = "its-a-secret-to-everyone"
app.config['DEBUG'] = False

qa = QA()
questionMaster = qa.returnQuestion()
wrong_answers_master = questionMaster[0][2]
seed = int()
hasAnswered = False

def initiateNewQuestion():
    global questionMaster
    global wrong_answers_master
    global seed
    global hasAnswered

    questionMaster = qa.returnQuestion()
    wrong_answers_master = questionMaster[0][2]
    seed = random.choice(range(120000))
    hasAnswered = False

@app.route("/", methods=['GET', 'POST'])
def index():
    global seed
    global questionMaster
    global wrong_answers_master
    global hasAnswered

    random.seed(seed)
    form = answerForm(request.form)
    question = questionMaster[0]
    correct_answer = question[1]
    abstract = None
    scoreHandler = list()
    multiple_answers = list()
    question_nature = questionMaster[1]


    # Comparative questions block
    if question_nature == "comparative":
        # comparative questions produce their own wrong answers (the third element from their return tuple)
        wrong_answers = wrong_answers_master.copy()
        # because we allow the user to answer using the number of the question, we choose randomly what this number is,
        # then we fill the other numbers with the wrong answers
        correct_answer_position = random.choice(range(1,4))
        for i in range(1,4):
            if i == correct_answer_position:
                # print(f"{i} : {correct_answer}")
                multiple_answers.append(correct_answer)
            else:
                # print(f"{i} : {wrong_answers.pop()}")
                multiple_answers.append(wrong_answers.pop())
        m_correct_answer = correct_answer_position

    # extracted question block
    if question_nature == "extracted":
        # print(question[2])
        abstract = question[2]
        wrong_answers = list(question[3])
        correct_answer_position = random.choice(range(1,5))
        for i in range(1,5):
            if i == correct_answer_position:
                # print(f"{i} : {correct_answer}")
                multiple_answers.append(correct_answer)
            else:
                # print(f"{i} : {wrong_answers.pop()}")
                multiple_answers.append(wrong_answers.pop())
        m_correct_answer = correct_answer_position


    # Multiple answers block
    # by nature, completion question cannot provide a correct answer, thus, it is only possible to give a direct answer
    elif question_nature != "completion" and question_nature != "comparative":
        wrong_answers = []
        # this block choose 3 wrong answers that are not identical to the correct answer to serve as noise
        while(len(wrong_answers) < 3):
            possible_wrong_answer = random.choice(qa.data)[question[2]]
            if possible_wrong_answer not in wrong_answers and possible_wrong_answer != None and possible_wrong_answer != correct_answer:
                wrong_answers.append(possible_wrong_answer)

        # fundamentally the same idea as for the comparatice questions block, we choose a random correct position,
        # and fill the wrong positions with noise
        correct_answer_position = random.choice(range(1,5))
        for i in range(1,5):
            if i == correct_answer_position:
                # print(f"{i} : {correct_answer}")
                multiple_answers.append(correct_answer)
            else:
                # print(f"{i} : {wrong_answers.pop()}")
                multiple_answers.append(wrong_answers.pop())
        
        m_correct_answer = correct_answer_position



    if form.validate_on_submit():
        if form.nextQuestion.data:
            initiateNewQuestion()
            return redirect(url_for('index'))
        if hasAnswered:
            initiateNewQuestion()
            return redirect(url_for('index'))
        user_input = form.input.data
        hasAnswered = True
        # Answer processing block
        if question_nature != "completion":
            # User input
            answer = user_input
            try:
                # first we try to process the answer as an integer, if the answer is exactly equal to the answer or it's position in the multiple answers
                # we call scoreHandler with True to process a correct answer
                # elsewise, we call it with False to process an incorrect answer
                ianswer = int(answer)
                if ianswer == correct_answer or ianswer == m_correct_answer:
                    if question_nature != "extracted":
                        scoreHandler.append(qa.returnScoreHandler(True, question[2]))
                    else:
                        scoreHandler.append(qa.returnScoreHandler(True))
                else:
                    if question_nature != "extracted":
                        scoreHandler.append(qa.returnScoreHandler(False, question[2]))
                    else:
                        scoreHandler.append(qa.returnScoreHandler(False))
                    
                    scoreHandler.append(f"The correct answer was {m_correct_answer} : {correct_answer}")
            except ValueError:
                # if the user didn't give an int as an answer, we check if the answer is close enough to the answer (60% similar)
                # to give some form of leeway in spelling mistakes
                # we call scoreHandler with true if the answer is close enough
                if not isinstance(correct_answer, int):
                    if fuzz.ratio(correct_answer, answer) > 60:
                        if question_nature != "extracted":
                            scoreHandler.append(qa.returnScoreHandler(True, question[2]))
                        else:
                            scoreHandler.append(qa.returnScoreHandler(True))
                    else:
                        if question_nature != "extracted":
                            scoreHandler.append(qa.returnScoreHandler(False, question[2]))
                        else:
                            scoreHandler.append(qa.returnScoreHandler(False))
                        
                        # scoreHandler.append(f"The expected answer was : {correct_answer}")
                else:
                    # This block should never be reached, but is kept just in case something catastrophic happens
                    scoreHandler.append(returnScoreHandler(False, question[2]))
                    
                    # scoreHandler.append(f"The expected answer was : {correct_answer} and you gave me a string.")
        # completion question block
        else:
            # User input
            answer = user_input
            if answer == "?":
                scoreHandler.append(f"Understood, abhorting question, your metascore is")
                scoreHandler.append(qa.metascore)
                
            else:
                scoreHandler.append("Thanks for your input, your metascore is")
                # scoreHandler.append(question[1])
                # scoreHandler.append(question[2])
                try:
                    qa.completer(str(answer), question[1], question[2])
                except ValueError:
                    pass
                scoreHandler.append(qa.metascore)
        

        return render_template('index.html', form=form, question=question[0], score = qa.score, multiple_answers=multiple_answers, abstract=abstract, scoreHandler=scoreHandler)
    return render_template('index.html', form=form, question=question[0], score = qa.score, multiple_answers=multiple_answers, abstract=abstract)



if __name__ == "__main__":
    app.run()